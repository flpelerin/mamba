CC		:= gcc
CFLAGS		:= -std=c99 -O3 -Ofast -march=native -fopenmp -static -Wall -Wextra
CLIBS		:= -lm -lc

MODELS_DIR	:= ./models
SCRIPTS_DIR	:= ./scripts

PYEXPORT	:= $(SCRIPTS_DIR)/export_model.py
SHDOWNLOAD	:= $(SCRIPTS_DIR)/download_hf.sh

C_MODEL		:= $(MODELS_DIR)/model.bin
C_MODEL_O	:= $(MODELS_DIR)/model.o
C_CONFIG	:= $(MODELS_DIR)/config.h

HF_MODEL	:= $(MODELS_DIR)/pytorch_model.bin
HF_CONFIG	:= $(MODELS_DIR)/config.json

HF_REPO		:= flpelerin/snake-91M
HF_SUBDIR	:= snake-c73af121-checkpoint-16
HF_FILES	:= config.json pytorch_model.bin training_params.json model.bin config.h

SRC		:= $(wildcard src/*.c)
TARGET		:= mamba




all: $(TARGET)

clean:
	rm -rf $(TARGET) *.o $(MODELS_DIR)

run: $(TARGET)
	./$(TARGET) -t .7 -n 256 -s 42 -p "Once upon a time, "




$(HF_MODEL):
	mkdir -p $(MODELS_DIR)
	$(SHDOWNLOAD) --path $(HF_REPO) --subdir $(HF_SUBDIR) --output $(MODELS_DIR) --files $(HF_FILES)

$(C_MODEL): $(HF_MODEL)
	if [ ! -f $(C_MODEL) ]; then \
		python3 $(PYEXPORT) --model_input_path=$(HF_MODEL) --config_input_path=$(HF_CONFIG) \
						--model_output_path=$(C_MODEL) --config_output_path=$(C_CONFIG); \
	fi
	
$(C_MODEL_O): $(C_MODEL)
	objcopy --input-target binary \
    		--output-target elf64-x86-64 \
    		--binary-architecture i386 \
			--redefine-sym _binary_models_model_bin_start=_embedded_binary_model \
    		$< $@

$(TARGET): $(SRC) $(C_MODEL_O)
	$(CC) $(CFLAGS) -o $@ $^ $(CLIBS)

 


.PHONY: all clean run
