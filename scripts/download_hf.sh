#!/bin/bash


# Implement argument parser with:
#   -h, --help
#   -p, --path      (huggingface repository path)
#   -s, --subdir    (huggingface repository subdirectory)
#   -f, --files     (files to download)
#   -o, --output    (output directory, default is current directory)

# Implement downloadFromPath function that downloads files from huggingface repository path
downloadHfModel() {
    hf_repo="$1"
    hf_subdir="$2"
    output_dir="$3"
    hf_files=($4)

    mkdir -p "$output_dir"

    # should ignore all errors, even if some files are not found
    set +e

    for file in "${hf_files[@]}"; do
        echo "Downloading $file from https://huggingface.co/$hf_repo/resolve/main/$hf_subdir/$file"
        wget "https://huggingface.co/$hf_repo/resolve/main/$hf_subdir/$file"
        mv "$file" "$output_dir"
    done
}

# Implement checkDependencies function that checks if wget is installed
checkDependencies() {
    if ! [ -x "$(command -v wget)" ]; then
        echo 'Error: wget is not installed.' >&2
        exit 1
    fi
}

# Help with the new arguments
help() {
    echo "Usage: download_hf.sh --path [model_path] --subdir [subdir] --output [output_dir] --files [file 1] [file 2] ... [file n]"
    echo "Download files from huggingface model path"
    echo "Example: download_hf.sh state-spaces/mamba-130m mamba-130m pytorch_model.bin config.json"
    echo "Options:"
    echo "  -h, --help      Show this help message and exit"
    echo "  -p, --path      Huggingface repository path"
    echo "  -s, --subdir    Huggingface repository subdirectory"
    echo "  -o, --output    Output directory, default is current directory"
    echo "  -f, --files     Files to download"
}



# Parse arguments
while [[ $# -gt 0 ]]; do
    key="$1"
    case $key in
        -h|--help)
            help
            exit 0
            ;;
        -p|--path)
            hf_repo="$2"
            shift 2
            ;;
        -s|--subdir)
            hf_subdir="$2"
            shift 2
            ;;
        -o|--output)
            output_dir="$2"
            shift 2
            ;;
        -f|--files)
            shift 1
            hf_files="$@"
            break
            ;;
        *)
            echo "Unknown argument: $1"
            help
            exit 1
            ;;
    esac
done


# Check if required arguments are provided
if [ -z "$hf_repo" ] || [ -z "$hf_subdir" ] || [ -z "$output_dir" ] || [ -z "$hf_files" ]; then
    echo "Error: Missing required arguments"
    help
    exit 1
fi

checkDependencies
downloadHfModel "$hf_repo" "$hf_subdir" "$output_dir" "$hf_files"
exit 0