
import os
import struct
import argparse
import json

import numpy as np
import torch





def serialize_fp32(file, tensor):
    """ writes one fp32 tensor to file that is open in wb mode """
    d = tensor.detach().cpu().view(-1).to(torch.float32).numpy()
    b = struct.pack(f'{len(d)}f', *d)
    file.write(b)


def write_weights(file, model, key):
    """ writes the layer weights to file """
    print(f"writing {key} {list(model[key].shape)[::-1]}")
    serialize_fp32(file, model[key])


def write_layer_weights(file, model, layer, n_layers):
    """ writes the layer weights to file """
    print(f"writing {layer % n_layers} {list(model[layer % 0].shape)[::-1]}")
    for n in range(n_layers):
        serialize_fp32(file, model[layer % n])


def load_model(model_path):
    model = torch.load(model_path, map_location='cpu')

     # remove the 'backbone.' prefix from the keys
    unwanted_prefix = 'backbone.'
    for k,v in list(model.items()):
        if k.startswith(unwanted_prefix):
            model[k[len(unwanted_prefix):]] = model.pop(k)
    
    return model


def load_config(config_path):
    with open(config_path) as f:
        config = json.load(f)

    return config


def export_model(model, config, output_path):
    out_file = open(output_path, 'wb')

    n_layers = config['n_layer']

    '''
    Example of the model structure:
    embedding.weight - [50280, 768]
    layers.0.mixer.D - [1536]
    layers.0.mixer.in_proj.weight - [3072, 768]
    layers.0.mixer.conv1d.weight - [1536, 1, 4]
    layers.0.mixer.conv1d.bias - [1536]
    layers.0.mixer.x_proj.weight - [80, 1536]
    layers.0.mixer.dt_proj.weight - [1536, 48]
    layers.0.mixer.dt_proj.bias - [1536]
    layers.0.mixer.A_log - [1536, 16]
    layers.0.mixer.out_proj.weight - [768, 1536]
    layers.0.norm.weight - [768]
    norm_f.weight - [768]
    lm_head.weight - [50280, 768]
    '''

    for n in range(n_layers):
        model[f'layers.{n}.mixer.A'] = -torch.exp(model.pop(f'layers.{n}.mixer.A_log'))

    write_weights(out_file, model, 'embedding.weight')

    write_layer_weights(out_file, model, 'layers.%d.mixer.in_proj.weight', n_layers)
    write_layer_weights(out_file, model, 'layers.%d.mixer.conv1d.weight', n_layers)
    write_layer_weights(out_file, model, 'layers.%d.mixer.conv1d.bias', n_layers)
    write_layer_weights(out_file, model, 'layers.%d.mixer.x_proj.weight', n_layers)
    write_layer_weights(out_file, model, 'layers.%d.mixer.dt_proj.weight', n_layers)
    write_layer_weights(out_file, model, 'layers.%d.mixer.dt_proj.bias', n_layers)
    write_layer_weights(out_file, model, 'layers.%d.mixer.A', n_layers)
    write_layer_weights(out_file, model, 'layers.%d.mixer.D', n_layers)
    write_layer_weights(out_file, model, 'layers.%d.mixer.out_proj.weight', n_layers)
    write_layer_weights(out_file, model, 'layers.%d.norm.weight', n_layers)

    write_weights(out_file, model, 'norm_f.weight')
    write_weights(out_file, model, 'lm_head.weight')

    out_file.close()
    print(f"Exported model to {output_path}")


def export_config(model, config, output_path):
    """
    Exports the config to a C header file, following this configuration example:
    
        #define N_LAYERS 12
        #define VOCAB_SIZE 256
        #define D_MODEL 768
        #define D_INNER 1536
        #define DT_RANK 48
        #define D_STATE 16
        #define D_CONV 4
    
    #define [KEY] [VALUE]
    key is converted to uppercase and value is the value from the config dictionary
    """

    with open(output_path, 'w') as f:
        f.write("#pragma once\n\n")
        f.write("#define N_LAYERS %d\n" % config['n_layer'])
        f.write("#define VOCAB_SIZE %d\n" % config['vocab_size'])
        f.write("#define D_MODEL %d\n" % config['d_model'])
        f.write("#define D_INNER %d\n" % (2 * config['d_model']))
        f.write("#define DT_RANK %d\n" % model['layers.0.mixer.dt_proj.weight'].shape[1])
        f.write("#define D_STATE %d\n" % model['layers.0.mixer.A'].shape[1])
        f.write("#define D_CONV %d\n" % model['layers.0.mixer.conv1d.weight'].shape[2])


    print(f"Exported config to {output_path}")





if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--model_input_path", type=str, help="path to the input model file")
    parser.add_argument("--model_output_path", type=str, help="path to the output model file")
    parser.add_argument("--config_input_path", type=str, help="path to the input config file")
    parser.add_argument("--config_output_path", type=str, help="path to the output config file")

    parser.set_defaults(model_input_path="pytorch_model.bin", model_output_path="model.bin")
    parser.set_defaults(config_input_path="config.json", config_output_path="config.h")
    args = parser.parse_args()

    model = load_model(args.model_input_path)
    config = load_config(args.config_input_path)

    export_model(model, config, args.model_output_path)
    export_config(model, config, args.config_output_path)