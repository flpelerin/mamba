#pragma once

#include <time.h>
#include <sys/time.h>
#include <stdlib.h>




typedef float fp32_t;
typedef signed char int8_t;
typedef unsigned char uint8_t;
typedef unsigned long long uint64_t;




typedef enum {false, true} bool;
static bool verbose = false;

#define LOG(...) fprintf(stderr, __VA_ARGS__)
#define CLOG(...) if (verbose) LOG(__VA_ARGS__)

#define PRINT(C) fputc((char)C, stdout), fflush(stdout)




static void help(char *name, void *defaults[]) {
    LOG("Usage: %s [-pntvh]\n\n", name);
    LOG("Infers a Mamba language model.\n\n");
    LOG("Options:\n");
    LOG("\t-p <prompt>          The prompt to start the generation with.    (default NONE)\n");
    LOG("\t-n <n_predict>       The number of tokens to predict.            (default %llu)\n", *(uint64_t *)defaults[0]);
    LOG("\t-t <temperature>     The temperature of the softmax.             (default %.1f)\n", *(fp32_t *)defaults[1]);
    LOG("\t-s <seed>            The seed for the random number generator.   (default %llu)\n", *(uint64_t *)defaults[2]);
    LOG("\t-v                   Enables verbose mode.                       (default: %s)\n", *(bool *)defaults[3] ? "true" : "false");
    LOG("\t-h                   Prints this help message.\n\n");

    exit(EXIT_FAILURE);
}




static uint64_t rng_seed = 0;
static uint64_t random_u32() { 
    rng_seed ^= rng_seed >> 12;
    rng_seed ^= rng_seed << 25;
    rng_seed ^= rng_seed >> 27;
    return (rng_seed * 0x2545F4914F6CDD1Dull) >> 32;
}
static inline fp32_t random_f32() { return (random_u32() >> 8) / 16777216.0f; }




static uint64_t time_in_ms() {
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return tv.tv_sec * 1000 + tv.tv_usec / 1000;
}




static char *shortScale(char *result, long number) {
    char *suffixes[] = {"", "k", "m", "b"};
    int magnitude = 0;
    double num = (double)number;

    if (number < 1000) {
        sprintf(result, "%ld", number);
        return result;
    }

    while (number >= 1000 || number <= -1000) {
        magnitude++;
        number /= 1000;
        num /= 1000.0;
    }

    sprintf(result, "%.0lf%s", num, suffixes[magnitude]);
    return result;
}