#pragma once

#include <stdio.h>
#include <string.h>
#include <math.h>

#include "../models/config.h"
#include "util.h"




extern char _embedded_binary_model[];




#define Tensor(X, Y, Z) [(X) * (Y) * (Z)]

typedef struct {
    uint64_t vocab_size; // vocabulary size, rounded to nearest multiple of 8
    uint64_t n_layers;   // number of layers
    uint64_t d_model;    // embedding dim
    uint64_t d_inner;
    uint64_t dt_rank;
    uint64_t d_state;
    uint64_t d_conv;
} MambaConfig;

typedef struct {
    fp32_t embed            Tensor(VOCAB_SIZE,      D_MODEL,                    1);
    fp32_t in_proj          Tensor(N_LAYERS,        2 * D_INNER,                D_MODEL);
    fp32_t conv1d_weight    Tensor(N_LAYERS,        D_INNER,                    D_CONV);
    fp32_t conv1d_bias      Tensor(N_LAYERS,        D_INNER,                    1);
    fp32_t x_proj           Tensor(N_LAYERS,        DT_RANK + (2 * D_STATE),    D_INNER);
    fp32_t dt_proj_weight   Tensor(N_LAYERS,        D_INNER,                    DT_RANK);
    fp32_t dt_proj_bias     Tensor(N_LAYERS,        D_INNER,                    1);
    fp32_t A                Tensor(N_LAYERS,        D_INNER,                    D_STATE);
    fp32_t D                Tensor(N_LAYERS,        D_INNER,                    1);
    fp32_t out_proj         Tensor(N_LAYERS,        D_MODEL,                    D_INNER);
    fp32_t norm             Tensor(N_LAYERS,        D_MODEL,                    1);
    fp32_t final_norm       Tensor(D_MODEL,         1,                          1);
    fp32_t lm_head          Tensor(VOCAB_SIZE,      D_MODEL,                    1);
} MambaWeights;

typedef struct {
    fp32_t input            Tensor(D_MODEL,                    1,               1);
    fp32_t hidden_state     Tensor(D_MODEL,                    1,               1);
    fp32_t xz               Tensor(2 * D_INNER,                1,               1);
    fp32_t x_db             Tensor(DT_RANK + (2 * D_STATE),    1,               1);
    fp32_t dt               Tensor(D_INNER,                    1,               1);
    fp32_t y                Tensor(D_INNER,                    1,               1);
    fp32_t logits           Tensor(VOCAB_SIZE,                 1,               1);
    fp32_t conv_state       Tensor(N_LAYERS,                   D_INNER,         D_CONV);
    fp32_t ssm_state        Tensor(N_LAYERS,                   D_INNER,         D_STATE);
} MambaState;

typedef struct {
    MambaConfig config;
    MambaWeights *weights;
    MambaState state;
} Mamba;




static Mamba mamba = {
    .config = {
        .vocab_size = VOCAB_SIZE,
        .n_layers = N_LAYERS,
        .d_model = D_MODEL,
        .d_inner = D_INNER,
        .dt_rank = DT_RANK,
        .d_state = D_STATE,
        .d_conv = D_CONV,
    },

    .weights = (MambaWeights *) _embedded_binary_model

};




static void LogMambaConfig() {
    CLOG("Mamba Config:\n");
    CLOG("  vocab_size:     %13llu\n",          mamba.config.vocab_size);
    CLOG("  n_layers:       %13llu\n",          mamba.config.n_layers);
    CLOG("  d_model:        %13llu\n",          mamba.config.d_model);
    CLOG("  d_inner:        %13llu\n",          mamba.config.d_inner);
    CLOG("  dt_rank:        %13llu\n",          mamba.config.dt_rank);
    CLOG("  d_state:        %13llu\n",          mamba.config.d_state);
    CLOG("  d_conv:         %13llu\n\n",        mamba.config.d_conv);

    char buffer[16];
    
    CLOG("Parameters Count:\n");
    CLOG("  embed:          %13lu (%s)\n",      sizeof(mamba.weights->embed)            / sizeof(fp32_t), shortScale(buffer, sizeof(mamba.weights->embed)           / sizeof(fp32_t)));
    CLOG("  in_proj:        %13lu (%s)\n",      sizeof(mamba.weights->in_proj)          / sizeof(fp32_t), shortScale(buffer, sizeof(mamba.weights->in_proj)         / sizeof(fp32_t)));
    CLOG("  conv1d_weight:  %13lu (%s)\n",      sizeof(mamba.weights->conv1d_weight)    / sizeof(fp32_t), shortScale(buffer, sizeof(mamba.weights->conv1d_weight)   / sizeof(fp32_t)));
    CLOG("  conv1d_bias:    %13lu (%s)\n",      sizeof(mamba.weights->conv1d_bias)      / sizeof(fp32_t), shortScale(buffer, sizeof(mamba.weights->conv1d_bias)     / sizeof(fp32_t)));
    CLOG("  x_proj:         %13lu (%s)\n",      sizeof(mamba.weights->x_proj)           / sizeof(fp32_t), shortScale(buffer, sizeof(mamba.weights->x_proj)          / sizeof(fp32_t)));
    CLOG("  dt_proj_weight: %13lu (%s)\n",      sizeof(mamba.weights->dt_proj_weight)   / sizeof(fp32_t), shortScale(buffer, sizeof(mamba.weights->dt_proj_weight)  / sizeof(fp32_t)));
    CLOG("  dt_proj_bias:   %13lu (%s)\n",      sizeof(mamba.weights->dt_proj_bias)     / sizeof(fp32_t), shortScale(buffer, sizeof(mamba.weights->dt_proj_bias)    / sizeof(fp32_t)));
    CLOG("  A:              %13lu (%s)\n",      sizeof(mamba.weights->A)                / sizeof(fp32_t), shortScale(buffer, sizeof(mamba.weights->A)               / sizeof(fp32_t)));
    CLOG("  D:              %13lu (%s)\n",      sizeof(mamba.weights->D)                / sizeof(fp32_t), shortScale(buffer, sizeof(mamba.weights->D)               / sizeof(fp32_t)));
    CLOG("  out_proj:       %13lu (%s)\n",      sizeof(mamba.weights->out_proj)         / sizeof(fp32_t), shortScale(buffer, sizeof(mamba.weights->out_proj)        / sizeof(fp32_t)));
    CLOG("  norm:           %13lu (%s)\n",      sizeof(mamba.weights->norm)             / sizeof(fp32_t), shortScale(buffer, sizeof(mamba.weights->norm)            / sizeof(fp32_t)));
    CLOG("  final_norm:     %13lu (%s)\n",      sizeof(mamba.weights->final_norm)       / sizeof(fp32_t), shortScale(buffer, sizeof(mamba.weights->final_norm)      / sizeof(fp32_t)));
    CLOG("  lm_head:        %13lu (%s)\n\n",    sizeof(mamba.weights->lm_head)          / sizeof(fp32_t), shortScale(buffer, sizeof(mamba.weights->lm_head)         / sizeof(fp32_t)));

    CLOG("Total:            %13lu (%s)\n\n",    sizeof(MambaWeights)                    / sizeof(fp32_t), shortScale(buffer, sizeof(MambaWeights)                   / sizeof(fp32_t)));
}




static void rmsnorm(fp32_t* y, fp32_t* x, fp32_t* weight, uint64_t size) {
    fp32_t ss = 0.0f;
    for (uint64_t j = 0; j < size; ++j)
        ss += x[j] * x[j];

    ss /= size;
    ss += 1e-5f;
    ss = 1.0f / sqrtf(ss);

    for (uint64_t j = 0; j < size; ++j)
        y[j] = x[j] * weight[j] * ss;
}

static void softmax(fp32_t* x, uint64_t size) {
    fp32_t max_val = x[0];
    for (uint64_t i = 1; i < size; ++i)
        if (x[i] > max_val) max_val = x[i];

    fp32_t sum = 0.0f;
    for (uint64_t i = 0; i < size; ++i) {
        x[i] = expf(x[i] - max_val);
        sum += x[i];
    }

    for (uint64_t i = 0; i < size; ++i)
        x[i] /= sum;
}

static fp32_t softplus(fp32_t x) { return logf(1.0f + expf(x)); }
static fp32_t sigmoid(fp32_t x) { return 1.0f / (1.0f + expf(-x)); }
static fp32_t silu(fp32_t x) { return x * sigmoid(x); }

static void shift_and_update_last_column(fp32_t* matrix, fp32_t* x, uint64_t rows, uint64_t cols) {
    #pragma omp parallel for
    for (uint64_t i = 0; i < rows; ++i) {
        fp32_t* row = matrix + i * cols;

        for (uint64_t j = 0; j < cols - 1; ++j)
            row[j] = row[j + 1];

        row[cols - 1] = x[i];
    }
}

static void conv1d_silu(fp32_t* x, fp32_t* conv_state, fp32_t* conv1d_weight, fp32_t* conv1d_bias, uint64_t d_inner, uint64_t d_conv) {
    #pragma omp parallel for
    for (uint64_t i = 0; i < d_inner; ++i) {
        fp32_t val = 0.0f;

        for (uint64_t j = 0; j < d_conv; ++j) {
            uint64_t index = i * d_conv + j;
            val += conv_state[index] * conv1d_weight[index];
        }

        x[i] = silu(val + conv1d_bias[i]);
    }
}

static void matmul(fp32_t* y, fp32_t* x, fp32_t* w, uint64_t d, uint64_t n) {
    #pragma omp parallel for
    for (uint64_t i = 0; i < d; ++i) {
        fp32_t val = 0.0f;

        for (uint64_t j = 0; j < n; ++j)
            val += w[i * n + j] * x[j];

        y[i] = val;
    }
}

static void dense_softplus(fp32_t* y, fp32_t* x, fp32_t* w, fp32_t* b, uint64_t d, uint64_t n) {

    #pragma omp parallel for
    for (uint64_t i = 0; i < d; ++i) {
        fp32_t val = 0.0f;

        for (uint64_t j = 0; j < n; ++j)
            val += w[i * n + j] * x[j];

        y[i] = softplus(val + b[i]);
    }
}

static void selective_scan(fp32_t* y, fp32_t* ssm_state, fp32_t* dt, fp32_t* A, fp32_t* B, fp32_t* C, fp32_t* D, fp32_t* x, fp32_t* z, uint64_t d_inner, uint64_t d_state) {
    #pragma omp parallel for
    for (uint64_t i = 0; i < d_inner; ++i) {
        fp32_t val = 0.0f;

        for (uint64_t j = 0; j < d_state; ++j) {
            uint64_t index = i * d_state + j;

            fp32_t dA = expf(dt[i] * A[index]);
            fp32_t dB = dt[i] * B[j];

            ssm_state[index] = ssm_state[index] * dA + x[i] * dB;

            val += ssm_state[index] * C[j];
        }

        val += D[i] * x[i];
        y[i] = val * silu(z[i]);
    }
}




static void ForwardMambaLayer(uint64_t layer) {
    Mamba           *m = &mamba;
    MambaConfig     *p = &m->config;
    MambaWeights    *w = m->weights;
    MambaState      *s = &m->state;

    uint64_t        d_model = p->d_model,
                    d_inner = p->d_inner,
                    d_conv = p->d_conv,
                    d_state = p->d_state,
                    dt_rank = p->dt_rank;

    fp32_t          *hidden_state = s->hidden_state,
                    *conv_state = s->conv_state + layer * d_inner * d_conv,
                    *ssm_state = s->ssm_state + layer * d_inner * d_state;

    fp32_t          *y = s->y,
                    *xz = s->xz,
                    *x = xz,
                    *z = xz + d_inner,
                    *x_db = s->x_db,
                    *dt = x_db,
                    *B = x_db + dt_rank,
                    *C = x_db + dt_rank + d_state;


    // Proj input
    matmul(s->xz, hidden_state, w->in_proj + layer * 2*d_inner*d_model, 2*d_inner, d_model);


    // Conv
    shift_and_update_last_column(conv_state, x, d_inner, d_conv);
    conv1d_silu(x, conv_state, w->conv1d_weight + layer*d_inner*d_conv, w->conv1d_bias + layer*d_inner, d_inner, d_conv);


    // SSM
    matmul(s->x_db, x, w->x_proj + layer*(dt_rank+2*d_state)*d_inner, dt_rank+2*d_state, d_inner);
    dense_softplus(s->dt, dt, w->dt_proj_weight + layer*d_inner*dt_rank, w->dt_proj_bias + layer*d_inner, d_inner, dt_rank);
    dt = s->dt;  // NOTE: dt is now bigger: (d_inner) instead of (dt_rank)

    selective_scan(y, ssm_state, dt, w->A + layer*d_inner*d_state, B, C, w->D + layer*d_inner, x, z, d_inner, d_state);


    // Proj output
    matmul(hidden_state, y, w->out_proj + layer*d_model*d_inner, d_model, d_inner);

}

static fp32_t *ForwardMamba(uint64_t token) {
    Mamba           *m = &mamba;
    MambaConfig     *p = &m->config;
    MambaState      *s = &m->state;

    uint64_t        d_model = p->d_model,
                    n_layers = p->n_layers,
                    vocab_size = p->vocab_size;

    fp32_t          *input = s->input,
                    *hidden_state = s->hidden_state;

    fp32_t          *content_row = m->weights->embed + token * d_model;

    memcpy(input, content_row, d_model * sizeof(fp32_t));

    for (uint64_t layer = 0; layer < n_layers; ++layer) {
        rmsnorm(hidden_state, input, m->weights->norm + layer * d_model, d_model);
        ForwardMambaLayer(layer);

        for (uint64_t i = 0; i < d_model; ++i) {
            hidden_state[i] += input[i];
            input[i] = hidden_state[i];
        }
    }

    rmsnorm(hidden_state, hidden_state, m->weights->final_norm, d_model);

    // IF USING SHARED CLASSIFIER
    //matmul(s->logits, hidden_state, m->weights->embed, vocab_size, d_model);
    matmul(s->logits, hidden_state, m->weights->lm_head, vocab_size, d_model);

    return s->logits;
}