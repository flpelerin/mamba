
#include <stdlib.h>

#include "mamba.h"
#include "util.h"
#include "sampler.h"




int main(int argc, char *argv[]) {

    char *prompt = NULL;
    uint64_t n_predict = 256;
    fp32_t temperature = .7;
    rng_seed = (uint64_t)time(NULL);


    for (int i = 1; i < argc; i++) {
        if (argv[i][0] != '-') {
            LOG("Invalid argument: %s\n", argv[i]);
            return 1;
        }

        switch (argv[i][1]) {
            case 'p':
                prompt = argv[++i];
                break;
            case 'n':
                n_predict = strtoull(argv[++i], NULL, 10);
                break;
            case 't':
                temperature = strtod(argv[++i], NULL);
                break;
            case 's':
                rng_seed = strtoull(argv[++i], NULL, 10);
                break;
            case 'v':
                verbose = 1;
                break;
            case 'h':
                goto help_;
                break;
            default:
                LOG("Invalid argument: %s\n", argv[i]);
                return 1;
        }
    }


    if (verbose)
        LogMambaConfig();    

    if (generate(prompt, n_predict, temperature) == EXIT_FAILURE)
        goto help_;

    return 0;


    help_:
        help(argv[0], (void *[]) {&n_predict, &temperature, &rng_seed, &verbose});        
}