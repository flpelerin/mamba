#include <stdio.h>

#include "util.h"




static uint64_t sample_argmax(fp32_t* probabilities, uint64_t n) {
    uint64_t max_i = 0;
    fp32_t max_p = probabilities[0];

    for (uint64_t i = 1; i < n; ++i)
        if (probabilities[i] > max_p)
            max_i = i, max_p = probabilities[i];

    return max_i;
}

static uint64_t sample_mult(fp32_t* probabilities, uint64_t n, fp32_t coin) {
    fp32_t cdf = 0.0f;

    for (uint64_t i = 0; i < n; ++i) {
        cdf += probabilities[i];

        if (coin < cdf) return i;
    }
    
    return n - 1;
}

static uint64_t sample(fp32_t* logits, fp32_t temperature) {
    uint64_t next;
    uint64_t vocab_size = mamba.config.vocab_size;
    
    if (temperature == 0.0f) next = sample_argmax(logits, vocab_size);
    else {
        for (uint64_t q = 0; q < mamba.config.vocab_size; ++q) 
            logits[q] /= temperature;

        softmax(logits, vocab_size);

        fp32_t coin = random_f32();
        next = sample_mult(logits, vocab_size, coin);
    }

    return next;
}




static bool generate(char *prompt, uint64_t n_predict, fp32_t temperature) {
    uint64_t tokens[n_predict],
             len,
             time_start = time_in_ms();

    if (prompt == NULL || (len = strlen(prompt)) == 0) return EXIT_FAILURE;

    for (uint64_t i = 0; i < len; ++i) {
        tokens[i] = (uint64_t)prompt[i];
        ForwardMamba(tokens[i]);
    
        PRINT(prompt[i]);
    }

    for (; len < n_predict; ++len) {
        fp32_t *logits = ForwardMamba(tokens[len - 1]);
        uint64_t next = sample(logits, temperature);
        tokens[len] = next;

        PRINT((char)next);
    }

    LOG("\nachieved tok/s: %f\n", n_predict / (double)(time_in_ms() - time_start) * 1000);
    return EXIT_SUCCESS;
}
